# DAZN interview test

## Install it

```
npm install
```

## Run it

```
npm start
```

## Play it

```
http://localhost:3000
```

## A few notes

- I am Angular background but this task explicitely said No Angular. So I've tried to play it with a bit of fun creative vanilla JavaScript/TypeScript with almost no dependencies;
- The only non-dev dependency here is: 'normalize.css' for style consistency;
- I tried to make the code self-explanatory as much as possible. When there is no comment in specific code places, that means I think the readability is already good enough from the code naming, flow and structure;
- No unit tests here just to cut off a bit of time on this task. As I've spend already 3 hours;
- In my current projects I usually do unit tests with Jasmine + Karma with Chrome launcher, E2E tests with Jasmine + Protractor configured to run in multi browsers simutaniously;
- No separation development/production environments here for simplicity;
- No separation for view model / data model for simplicity;
- No extra features that I was planned to do. e.g. A popup after select one movie, feature rich pagination control, extra info fields for movie cards etc.
- Dependency Injection involved only services here for simplicity. In a real project UI components should be injectable as well by making proper component factories work with the DI container.
- Any further issues please do not hesitate to contact me.
