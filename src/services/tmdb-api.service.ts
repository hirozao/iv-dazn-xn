import { injectable } from 'inversify';
import { Config } from '../config';

@injectable()
export class TmdbApiService {
  private baseUrl = Config.tmdbApiBaseUrl;
  private apiKey = Config.tmdbApiKey;

  get<T>(path: string, params?: any): Promise<T> {
    return this.http(path, 'GET', params);
  }

  post<T>(path: string, body?: any, params?: any): Promise<T> {
    return this.http(path, 'POST', params, body);
  }

  put<T>(path: string, body?: any, params?: any): Promise<T> {
    return this.http(path, 'PUT', params, body);
  }

  delete<T>(path: string, params?: any): Promise<T> {
    return this.http(path, 'DELETE', params);
  }

  private http<T>(path: string, method: string, params?: any, body?: any): Promise<T> {
    let queryString = '';

    if (params) {
      Object.keys(params).forEach(key => (queryString += '&' + key + '=' + params[key]));
    }

    return fetch(this.baseUrl + path + '?api_key=' + this.apiKey + queryString, {
      method,
      body,
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(res => {
      if (res.ok) {
        return res.json();
      } else {
        return Promise.reject('http error');
      }
    });
  }
}
