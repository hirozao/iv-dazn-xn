import { injectable, inject } from 'inversify';
import { Movie } from '../models/movie';
import { TmdbApiService } from './tmdb-api.service';

@injectable()
export class MovieService {
  // demonstration for Dependency injection
  constructor(@inject(TmdbApiService) private api: TmdbApiService) {}

  search(
    query: string,
    page = 1
  ): Promise<{
    page: number;
    results: Movie[];
    total_pages: number;
    total_results: number;
  }> {
    return this.api.get('search/movie', { query, page });
  }
}
