/**
 * A component is a encapulated entity that renders UI piece based on view models.
 * This is a structure designed from scratch specifically for this test.
 */
export interface Component {
  /**
   * $container is the root DOM element for this component,
   * prefix $ indicate this is a DOM element variable
   */
  $container: HTMLElement | undefined;

  /**
   * renders DOM elements from view models
   */
  render(): void;

  /**
   * digest triggers a view model changing & refresh lifecycle to update DOM content
   */
  digest(): void;

  /**
   * attach component to a outside host DOM element
   * @param target host DOM element or css selector
   */
  attach(target: Element | string | null): void;
}

export class BaseComponent implements Component {
  $container: HTMLElement | undefined;

  constructor() {}

  render() {}
  digest() {}

  attach(target: Element | string | null) {
    if (typeof target === 'string') {
      target = document.querySelector<HTMLElement>(target);
    }

    if (target === null) {
      return;
    }

    if (!this.$container) {
      return;
    }

    target.appendChild(this.$container);
  }
}
