import { Container } from 'inversify';
import { TmdbApiService } from './services/tmdb-api.service';
import { MovieService } from './services/movie.service';

const myContainer = new Container();
myContainer.bind<TmdbApiService>(TmdbApiService).toSelf();
myContainer.bind<MovieService>(MovieService).toSelf();

export { myContainer };
