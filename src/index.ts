import 'normalize.css';
import './style.scss';
import 'reflect-metadata';

import { AppComponent } from './components/app.component';
import { myContainer } from './inversify.config';
import { TmdbApiService } from './services/tmdb-api.service';
import { MovieService } from './services/movie.service';

// IoC container
const tmdbApiService = myContainer.get<TmdbApiService>(TmdbApiService);
const movieService = myContainer.get<MovieService>(MovieService);

// bootstrap app
const app = new AppComponent(movieService);
app.attach('#app');
