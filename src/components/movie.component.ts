import { BaseComponent } from '../models/component';
import { Movie } from '../models/movie';
import { injectable } from 'inversify';

// in real project this may get from configuration endpoint of tmdb api, or from a config file
const PosterPath = 'https://image.tmdb.org/t/p/w500';

@injectable()
export class MovieComponent extends BaseComponent {
  movie: Movie;
  onClick = () => {};

  constructor(movie: Movie) {
    super();

    // assign view model
    this.movie = movie;

    this.render();
  }

  render() {
    this.$container = document.createElement('li');
    this.$container.className = 'movie-component';
    this.$container.onclick = () => this.onClick();

    // render movie thumb
    const $moviePoster = document.createElement('img');
    $moviePoster.src = this.movie.poster_path
      ? PosterPath + this.movie.poster_path
      : 'https://via.placeholder.com/200x300.png?text=no%20poster';

    // render movie title
    const $movieTitle = document.createElement('h3');
    $movieTitle.innerText = this.movie.title;

    // render movie description
    // const $movieDescription = document.createElement('p');
    // $movieDescription.innerText = this.movie.overview;

    this.$container.appendChild($moviePoster);
    this.$container.appendChild($movieTitle);
    // this.$container.appendChild($movieDescription);
  }
}
