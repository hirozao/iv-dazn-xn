import { BaseComponent } from '../models/component';
import { injectable } from 'inversify';
import { MovieComponent } from './movie.component';
import { Movie } from '../models/movie';
import { MovieService } from '../services/movie.service';

@injectable()
export class AppComponent extends BaseComponent {
  $input: HTMLInputElement;
  $movieList: HTMLElement;
  $controls: HTMLElement;
  $nextButton: HTMLElement;
  $prevButton: HTMLElement;

  movies: Movie[] = [];
  page: number = 0;
  totalPages: number = 0;
  query = '';

  constructor(private movieService: MovieService) {
    super();
    this.render();
  }

  render() {
    this.$container = document.createElement('div');
    this.$container.classList.add('index-page-component');

    // render movie list
    this.$movieList = document.createElement('ul');
    this.$movieList.className = 'movie-list';

    // render control wrapper
    this.$controls = document.createElement('div');
    this.$controls.className = 'controls';
    this.$controls.style.display = 'none';

    // render control buttons
    this.$nextButton = document.createElement('button');
    this.$nextButton.innerText = 'Next page';
    this.$nextButton.onclick = () => this.nextPage();
    this.$prevButton = document.createElement('button');
    this.$prevButton.innerText = 'Prev page';
    this.$prevButton.onclick = () => this.prevPage();

    this.$controls.appendChild(this.$prevButton);
    this.$controls.appendChild(this.$nextButton);

    // render search box
    this.$input = document.createElement('input');
    this.$input.type = 'text';
    this.$input.placeholder = 'Find amazing movies here...';
    this.$input.classList.add('search-box');
    this.$input.onkeypress = ev => {
      if (ev.keyCode === 13) {
        this.search(this.$input.value);
      }
    };

    // render search button
    const $button = document.createElement('button');
    $button.innerText = 'Search';
    $button.classList.add('search-btn');
    $button.onclick = () => this.search(this.$input.value);

    // add elements to container
    this.$container.appendChild(this.$input);
    this.$container.appendChild($button);
    this.$container.appendChild(this.$movieList);
    this.$container.appendChild(this.$controls);
  }

  digest() {
    if (this.page === 1) {
      this.$prevButton.style.display = 'none';
    } else {
      this.$prevButton.style.display = 'inline-block';
    }

    if (this.page === this.totalPages) {
      this.$nextButton.style.display = 'none';
    } else {
      this.$nextButton.style.display = 'inline-block';
    }

    if (this.movies.length === 0) {
      this.$movieList.style.display = 'none';
      this.$controls.style.display = 'none';
      alert('Oops! Nothing found.');
    } else {
      this.$movieList.style.display = 'flex';
      this.$controls.style.display = 'flex';
    }

    this.$input.value = this.query;

    this.movies.forEach(movie => {
      const movieComponent = new MovieComponent(movie);
      movieComponent.onClick = () => alert('Not implemented...');
      movieComponent.attach(this.$movieList);
    });
  }

  search(query: string, page = 1) {
    if (!query) {
      alert('Please input a keyword to start searching');
      return;
    }

    // clear existing list
    this.$movieList.innerHTML = '';

    // search movie by keyword
    this.movieService
      .search(query, page)
      .then(res => {
        // console.log('res', res);

        // preserve view models
        this.movies = res.results;
        this.page = res.page;
        this.totalPages = res.total_pages;
        this.query = query;

        // digest view models
        this.digest();
      })
      .catch(err => {
        // console.error(err);
      });
  }

  nextPage() {
    this.search(this.query, ++this.page);
  }

  prevPage() {
    this.search(this.query, --this.page);
  }
}
